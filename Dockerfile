FROM ubuntu:18.04

RUN apt-get update && apt-get -y install git maven

ADD ["appz_bot_example", "/home/"]

ENTRYPOINT ["/bin/sh", "-c", "chmod +x /home/telebot.sh && /home/telebot.sh"]
